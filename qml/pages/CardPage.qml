/*
  Copyright (C) 2013 Franck Routier, Mecadu, the doubting mechanism
  Contact: Franck Routier <alci@mecadu.org>
  All rights reserved.

  This file is part of MkPokerPlanning.

  MkPokerPlanning is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  MkPokerPlanning is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with MkPokerPlanning.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.0
// this is needed as we use the Page type
import Sailfish.Silica 1.0
import "../components/"


Page {
    id: cardPage
    property string backImageSource
    property string value
    property string backname

    property AppSettings appSettings
    property CardBacksLibrary backsLibrary

    SilicaFlickable {

        anchors.fill: parent

        PullDownMenu {
            MenuItem {
                text: qsTr("About this card back...")
                onClicked: {
                    pageStack.push(Qt.resolvedUrl("AboutBackPage.qml"),{"backname": backname, "backsLibrary": backsLibrary})
                }
            }
        }

        PageHeader {
            id: header
            title: qsTr("Here is my bet...")
        }


        ShakeSensor {
            id: shakeSensor
            active: card.state === "back" && status===PageStatus.Active && Qt.application.active && appSettings.getShakeToReveal()

            onShaked: {
                if (card.state === "back") {
                    card.state = "front"
                }
            }

        }

        Column {

            anchors.centerIn: parent

            Card {
                id: card
                backImageSource: cardPage.backImageSource
                value: cardPage.value
                fullSize: true
            }
        }



        Timer {
            interval: 5000
            running: card.state==="back" && status===PageStatus.Active && Qt.application.active
            repeat: true
            onTriggered: card.shake()
        }
    }

    onStatusChanged: {
       if (status === PageStatus.Activating) {
           card.state = "back";
       }
    }
}

