/*
  Copyright (C) 2013 Franck Routier, Mecadu, the doubting mechanism
  Contact: Franck Routier <alci@mecadu.org>
  All rights reserved.

  This file is part of MkPokerPlanning.

  MkPokerPlanning is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  MkPokerPlanning is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with MkPokerPlanning.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.0
import Sailfish.Silica 1.0
import "../components/"


Page {
    id: aboutBackPage

    property string backname
    property CardBacksLibrary backsLibrary

    SilicaFlickable {
        id: flickable
        anchors.fill: parent

        clip: true
        contentHeight: column.height

        PageHeader {
            id: header
            title: qsTr("About this card back...")
        }


        Column {
            id: column

            anchors.top: header.bottom
            width: parent.width
            spacing: Theme.paddingMedium

            Label {
                anchors {
                            left: parent.left
                            right: parent.right
                            margins: Theme.paddingLarge
                }
                wrapMode: Text.WordWrap
                font.pixelSize: Theme.fontSizeMedium

                //horizontalAlignment: Text.Center
                text: qsTr("Name: ") +backname
            }
            Label {
                anchors {
                            left: parent.left
                            right: parent.right
                            margins: Theme.paddingLarge
                }
                wrapMode: Text.WordWrap
                font.pixelSize: Theme.fontSizeMedium

                //horizontalAlignment: Text.Center
                text: qsTr("Family: ") + backsLibrary.getCardBackFromName(backname).family
            }
            Label {
                anchors {
                            left: parent.left
                            right: parent.right
                            margins: Theme.paddingLarge
                }
                wrapMode: Text.WordWrap
                font.pixelSize: Theme.fontSizeMedium

                //horizontalAlignment: Text.Center
                text: qsTr("License: ") + backsLibrary.getCardBackFromName(backname).license
            }
            Label {
                anchors {
                            left: parent.left
                            right: parent.right
                            margins: Theme.paddingLarge
                }
                wrapMode: Text.WordWrap
                font.pixelSize: Theme.fontSizeMedium

                //horizontalAlignment: Text.Center
                text: qsTr("Author: ") + backsLibrary.getCardBackFromName(backname).author
            }
        }
    }
}
