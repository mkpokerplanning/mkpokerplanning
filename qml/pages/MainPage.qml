/*
  Copyright (C) 2013 Franck Routier, Mecadu, the doubting mechanism
  Contact: Franck Routier <alci@mecadu.org>
  All rights reserved.

  This file is part of MkPokerPlanning.

  MkPokerPlanning is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  MkPokerPlanning is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with MkPokerPlanning.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.0
import Sailfish.Silica 1.0
import "../components/"


Page {
    id: page

    property AppSettings settings: AppSettings{}
    property CardBacksLibrary backsLibrary: CardBacksLibrary{}

    SilicaFlickable {
        anchors.fill: parent
        leftMargin: page.width/30
        rightMargin: page.width/30

        PullDownMenu {
            MenuItem {
                text: qsTr("About poker planning...")
                onClicked: pageStack.push(Qt.resolvedUrl("AboutPage.qml"))
            }
            MenuItem {
                text: qsTr("Settings...")
                onClicked: {
                    pageStack.push(Qt.resolvedUrl("SettingsPage.qml"),{"appSettings": settings, "backsLibrary": backsLibrary})
                }
            }
        }

        SilicaGridView {
            id: gridView
            anchors.fill: parent
            cellHeight: page.height/4.9
            cellWidth: page.height/4.9/1.65

            model: CardDeckModel {
                id: deckModel
            }

            header: PageHeader {
                title: qsTr("Choose your card !")
            }
            delegate: Card {
                id: myCard

                backImageSource: ico
                value: val
                cardname: bname
                height: gridView.cellHeight-3
                fullSize: false

                onClicked: {
                    pageStack.push(Qt.resolvedUrl("CardPage.qml"),{"appSettings": settings,
                                       "backImageSource": myCard.backImageSource,
                                       "backname": myCard.cardname,
                                       "value": myCard.value,
                                       "backsLibrary": backsLibrary
                                      })
                }
            }
        }
    }

    onStatusChanged: {
       if (status === PageStatus.Activating) {
           deckModel.series = settings.getDeckValues()
           deckModel.customseries = settings.getCustomSeriesArray()
           deckModel.backname = backsLibrary.getCardBackFromName(settings.getDeckBack()).settingsName
           deckModel.backsource = backsLibrary.getCardBackFromName(deckModel.backname).source
           deckModel.init()
       }
   }
}


