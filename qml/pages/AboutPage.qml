/*
  Copyright (C) 2013 Franck Routier, Mecadu, the doubting mechanism
  Contact: Franck Routier <alci@mecadu.org>
  All rights reserved.

  This file is part of MkPokerPlanning.

  MkPokerPlanning is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  MkPokerPlanning is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with MkPokerPlanning.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.0
import Sailfish.Silica 1.0


Page {
    id: aboutPage



    SilicaFlickable {
        id: flickable
        anchors.fill: parent

        clip: true
        contentHeight: column.height

        PageHeader {
            id: header
            title: qsTr("About this app...")
        }


        Column {
            id: column

            anchors.top: header.bottom
            width: parent.width
            spacing: Theme.paddingMedium

            Label {
                width: parent.width
                wrapMode: Text.WordWrap

                horizontalAlignment: Text.Center
                text: qsTr("<b>Play poker planning with your scrum team</b>")
            }
            Label {
                anchors {
                            left: parent.left
                            right: parent.right
                            margins: Theme.paddingLarge
                }
                wrapMode: Text.WordWrap
                font.pixelSize: Theme.fontSizeMedium

                //horizontalAlignment: Text.Center
                text: qsTr("The reason to use Planning poker is to avoid the influence of the other participants. If a number is spoken, it can sound like a suggestion and influence the other participants' sizing. Planning poker should force people to think independently and propose their numbers simultaneously. This is accomplished by requiring that all participants show their card at the same time.")
            }

            Label {
                width: parent.width
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.Center
                font.pixelSize: Theme.fontSizeExtraSmall
                text: qsTr("\n\nVersion 1.1-1")
            }
            Label {
                width: parent.width
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.Center
                font.pixelSize: Theme.fontSizeExtraSmall
                text: qsTr("\nDistributed under GPLv3 license.\nSource code available on Gitorious\nhttps://gitorious.org/mkpokerplanning\n©2013-2014 mecadu, the doubting mechanism.")
            }
        }
    }
}
