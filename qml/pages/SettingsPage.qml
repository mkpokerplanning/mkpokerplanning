/*
  Copyright (C) 2013 Franck Routier, Mecadu, the doubting mechanism
  Contact: Franck Routier <alci@mecadu.org>
  All rights reserved.

  This file is part of MkPokerPlanning.

  MkPokerPlanning is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  MkPokerPlanning is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with MkPokerPlanning.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.0
import Sailfish.Silica 1.0
import "../components/"


Page {
    id: settingsPage

    property AppSettings appSettings
    property CardBacksLibrary backsLibrary

    PageHeader {
        title: qsTr("Settings")
    }

    function getValuesIndex() {
        if (appSettings.getDeckValues() === "std")
            return 0
        else if (appSettings.getDeckValues() === "fibonacci")
            return 1
        else if (appSettings.getDeckValues() === "fibocafe")
            return 1
        else return 3
    }

    SilicaFlickable {
        anchors.fill: parent
        leftMargin: settingsPage.width/30
        rightMargin: settingsPage.width/30
        topMargin: 120

        Column {
            width: parent.width
            height: parent.height

            ComboBox {
                id: valuesCombo
                width: parent.width
                label: qsTr("Deck values")
                currentIndex: -1

                menu: ContextMenu {
                    MenuItem {
                        text: qsTr("standard")
                        onClicked: {
                            appSettings.setDeckValues("std")
                        }
                    }
                    MenuItem {
                        text: qsTr("fibonacci")
                        onClicked: {
                            appSettings.setDeckValues("fibonacci")
                        }
                    }
                    MenuItem {
                        text: qsTr("fibonacci with coffee")
                        onClicked: {
                            appSettings.setDeckValues("fibocafe")
                        }
                    }
                    MenuItem {
                        text: qsTr("custom series")
                        onClicked: {
                            appSettings.setDeckValues("custom")
                        }
                    }
                }
            }

            TextField {
               id: custsertext
               visible: (appSettings.getDeckValues() === "custom")
               width: parent.width
               label: qsTr("Custom series")
               placeholderText: qsTr("1,2,3,short text,...")
               focus: false
               EnterKey.onClicked: {
                   appSettings.setCustomSeries(text)
                   parent.focus = true;
               }
            }

            ComboBox {
                id: backCombo
                width: parent.width
                label: qsTr("Deck back")

                menu: ContextMenu {

                    Repeater {
                        model: backsLibrary
                        delegate: MenuItem {
                            property string val: backsLibrary.get(index).settingsName
                            text: backsLibrary.description(index)
                        }
                    }
                }
                // Workaround for http://www.mail-archive.com/devel@lists.sailfishos.org/msg02026.html
                // onCurrentItemChanged didn't work with Repeater defined MenuItems...
                onCurrentIndexChanged: {
                    appSettings.setDeckBack(currentItem.val)
                }
            }

            TextSwitch {
                id: shakeSwitch
                text: qsTr("Shake to reveal")
                checked: true
                description: qsTr("Shake your Jolla to reveal your card !")

                onCheckedChanged: {
                    appSettings.setShakeToReveal(checked)
                }
            }
        }
    }

    onStatusChanged: {
        if (status === PageStatus.Activating) {
            valuesCombo.currentIndex = getValuesIndex()
            backCombo.currentIndex = backsLibrary.getBackIndex(appSettings.getDeckBack())
            shakeSwitch.checked = appSettings.getShakeToReveal()
            custsertext.text = appSettings.getCustomSeries()
        }
    }
}
