/*
  Copyright (C) 2013 Franck Routier, Mecadu, the doubting mechanism
  Contact: Franck Routier <alci@mecadu.org>
  All rights reserved.

  This file is part of MkPokerPlanning.

  MkPokerPlanning is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  MkPokerPlanning is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with MkPokerPlanning.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.0

ListModel {
    id: backsLibrary

    ListElement {
        settingsName: "random"
        source: "qrc:///backs/backold.png"
        description: QT_TR_NOOP("random")
        family: "all"
        license: "multiple"
        author: "multiple"
    }

    ListElement {
        settingsName: "old"
        source: "qrc:///backs/backold.png"
        description: QT_TR_NOOP("old style back")
        family: "historical"
        license: "CC-BY-SA"
        author: "Pressburger"
    }

    ListElement {
        settingsName: "swingorig"
        source: "qrc:///backs/swing-old.png"
        description: QT_TR_NOOP("swing project (orig.)")
        family: "humor"
        license: "Unknown"
        author: "S. Høgh"
    }

    ListElement {
        settingsName: "lifecycle-fr"
        source: "qrc:///backs/lifecycle-fr.png"
        description: QT_TR_NOOP("project lifecycle (fr)")
        family: "humor"
        license: "Unknown"
        author: "Unknown"
    }

    ListElement {
        settingsName: "brave-gnu-world"
        source: "qrc:///backs/Brave_GNU_world.png"
        description: QT_TR_NOOP("brave GNU world")
        family: "freesoftware"
        license: "CC-BY-SA-2.5"
        author: "David Batley"
    }

    ListElement {
        settingsName: "stallman"
        source: "qrc:///backs/stallman.png"
        description: QT_TR_NOOP("Stallman drawing")
        family: "freesoftware"
        license: "CC-BY-SA"
        author: "Lucy Watts"
    }

    ListElement {
        settingsName: "communism"
        source: "qrc:///backs/communism.png"
        description: QT_TR_NOOP("communism...")
        family: "freesoftware"
        license: "Unknown"
        author: "Unknown"
    }

    ListElement {
        settingsName: "eiffel"
        source: "qrc:///backs/eiffel.png"
        description: QT_TR_NOOP("eiffel tower")
        family: "engeniering"
        license: "Unknown"
        author: "Unknown"
    }

    ListElement {
        settingsName: "eiffel_plan"
        source: "qrc:///backs/Maurice_koechlin_pylone.png"
        description: QT_TR_NOOP("eiffel tower plan")
        family: "engeniering"
        license: "Public Domain (1884)"
        author: "Maurice Koechlin, Émile Nouguier"
    }

    function description(index) {
        return qsTr(backsLibrary.get(index).description);
    }

    function getCardBackFromName(name) {
        if (name === "random") {
            return getRandomCardBack()
        }
        else {
            for (var index = 1; index < backsLibrary.count; ++index) {
                if (backsLibrary.get(index).settingsName === name) {
                    return backsLibrary.get(index);
                }
            }
        }
    }

    function getRandomCardBack() {
        var rnd = Math.random()
        var step = 1/(backsLibrary.count-1)
        var idx = Math.floor(rnd / step)+1
        return backsLibrary.get(idx)
    }

    function getBackIndex(name) {
        for (var index = 0; index < backsLibrary.count; ++index) {
            if (backsLibrary.get(index).settingsName === name) {
                return index;
            }
        }

    }
}
