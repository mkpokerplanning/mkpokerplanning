/*
  Copyright (C) 2013 Franck Routier, Mecadu, the doubting mechanism
  Contact: Franck Routier <alci@mecadu.org>
  All rights reserved.

  This file is part of MkPokerPlanning.

  MkPokerPlanning is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  MkPokerPlanning is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with MkPokerPlanning.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.0
import Sailfish.Silica 1.0

Flipable {
    id: card
    signal clicked

    property string backImageSource
    property string value: "1"
    property string cardname
    // this is the canonical height, to compute relative size. 394x650
    property real canonicalHeight: 650
    property real ratio: 1.65
    property bool fullSize: true

    // will be overriden to fit actual layout
    height: canonicalHeight
    width: height/ratio

    property bool flipped: false

    front: Rectangle {

        radius: fullSize ? 15 : 5
        height: parent.height
        width: parent.width
        smooth: true

        Text {
            id: valueTextAngle
            text: value
            color: "black"
            font.family: Theme.fontFamily
            font.pointSize: fullSize ? Theme.fontSizeSmall : Theme.fontSizeTiny
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.leftMargin: fullSize ? 10 : 5
            anchors.topMargin: fullSize ? 5 : 2
        }

        Text {
            id: valueTextBottomAngle
            text: value
            color: "black"
            font.family: Theme.fontFamily
            font.pointSize: fullSize ? Theme.fontSizeSmall : Theme.fontSizeTiny
            rotation: 180
            anchors.bottom: parent.bottom
            anchors.right: parent.right
            anchors.rightMargin: fullSize ? 10 : 5
            anchors.bottomMargin: fullSize ? 5 : 2
        }

        Text {
            id: valueTextMain
            text: value
            font.pointSize: Theme.fontSizeLarge * (fullSize ? 3 : 1)
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            width: parent.width
            elide: Text.ElideRight
            wrapMode: Text.WordWrap
            horizontalAlignment: Text.Center
            color: "black"
            font.family: Theme.fontFamily
            font.bold: true
        }

        Image {
            source: backImageSource
            anchors.fill: parent
            fillMode: Image.Stretch
            opacity: 0.2
        }

        MouseArea {
            anchors.fill: parent
            onClicked: card.clicked()
        }
    }


    back:  Rectangle {

        height: parent.height
        width: parent.width
        color: "transparent"
        smooth: true
        antialiasing: true

        Image {
            id: fond
            source: backImageSource
            anchors.fill: parent
            fillMode: Image.Stretch
            opacity: 1
        }

        MouseArea {
            anchors.fill: parent
            onClicked: card.clicked()
        }
    }

    transform: Rotation {
        id: rotation
        origin.x: card.width/2
        origin.y: card.height/2
        axis.x: 0; axis.y: 1; axis.z: 0
        angle: 0
    }

    states: [
        State {
            name: "front"
            PropertyChanges {target: card; flipped: false}
            PropertyChanges { target: rotation; angle: 0 }

        },
        State {
            name: "back"
            PropertyChanges {target: card; flipped: true}
            PropertyChanges { target: rotation; angle: 180 }

        }
    ]

    transitions: Transition {
        NumberAnimation { target: rotation; property: "angle"; duration: 700 }
    }

    onClicked: {
        if (fullSize) {
            if (card.state === "front")
                card.state = "back"
            else {
                card.state = "front"
            }
        }
    }

    function shake() {
        shakeAnimation.running = true && Qt.application.active
    }

    SequentialAnimation {
        id: shakeAnimation
        running: false && Qt.application.active
        RotationAnimation {target: card; duration: 70; from: 0; to: 5}
        RotationAnimation {target: card; duration: 70; from: 5; to: -5}
        RotationAnimation {target: card; duration: 70; from: -5; to: 5}
        RotationAnimation {target: card; duration: 70; from: 5; to: -5}
        RotationAnimation {target: card; duration: 70; from: -5; to: 5}
        RotationAnimation {target: card; duration: 70; from: 5; to: -5}
        RotationAnimation {target: card; duration: 70; from: -5; to: 5}
        RotationAnimation {target: card; duration: 70; from: 5; to: -5}
        RotationAnimation {target: card; duration: 70; from: -5; to: 5}
        RotationAnimation {target: card; duration: 70; from: 5; to: -5}
        RotationAnimation {target: card; duration: 70; from: -5; to: 5}
        RotationAnimation {target: card; duration: 70; from: 5; to: -5}
        RotationAnimation {target: card; duration: 70; from: -5; to: 5}
        RotationAnimation {target: card; duration: 70; from: 5; to: 0}
    }
}
