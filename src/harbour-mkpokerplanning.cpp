/*
  Copyright (C) 2013 Franck Routier, Mecadu, the doubting mechanism
  Contact: Franck Routier <alci@mecadu.org>
  All rights reserved.

  This file is part of MkPokerPlanning.

  MkPokerPlanning is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  MkPokerPlanning is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with MkPokerPlanning.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifdef QT_QML_DEBUG
#include <QtQuick>
#endif

#include <QGuiApplication>
#include <QQuickView>
#include <sailfishapp.h>
#include <QTranslator>


int main(int argc, char *argv[])
{
    //   - SailfishApp::application(int, char *[]) to get the QGuiApplication *
    QGuiApplication *app = SailfishApp::application(argc, argv);
    //   - SailfishApp::createView() to get a new QQuickView * instance
    QQuickView *view = SailfishApp::createView();

    QTranslator translator;
    QString locale = QLocale::system().name().section('_', 0, 0);
    translator.load(QString("mkpokerplanning_")+locale,"/usr/share/harbour-mkpokerplanning/languages/");
    app->installTranslator(&translator);

    //QmlSettings *settings = new QmlSettings();
    //view->rootContext()->setContextProperty("settings", settings);

    //   - SailfishApp::pathTo(QString) to get a QUrl to a resource file
    view->setSource(SailfishApp::pathTo("qml/harbour-mkpokerplanning.qml"));
    // To display the view, call "show()" (will show fullscreen on device).
    view->show();
    return app->exec();
}

