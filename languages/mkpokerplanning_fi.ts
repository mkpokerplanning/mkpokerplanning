<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fi_FI">
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="40"/>
        <source>About this app...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="56"/>
        <source>&lt;b&gt;Play poker planning with your scrum team&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="68"/>
        <source>The reason to use Planning poker is to avoid the influence of the other participants. If a number is spoken, it can sound like a suggestion and influence the other participants&apos; sizing. Planning poker should force people to think independently and propose their numbers simultaneously. This is accomplished by requiring that all participants show their card at the same time.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="76"/>
        <source>

Version 1.1-1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AboutPage.qml" line="83"/>
        <source>
Distributed under GPLv3 license.
Source code available on Gitorious
https://gitorious.org/mkpokerplanning
©2013-2014 mecadu, the doubting mechanism.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CardBacksLibrary</name>
    <message>
        <location filename="../qml/components/CardBacksLibrary.qml" line="30"/>
        <source>random</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/CardBacksLibrary.qml" line="39"/>
        <source>old style back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/CardBacksLibrary.qml" line="48"/>
        <source>swing project (orig.)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/CardBacksLibrary.qml" line="57"/>
        <source>project lifecycle (fr)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/CardBacksLibrary.qml" line="66"/>
        <source>brave GNU world</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/CardBacksLibrary.qml" line="75"/>
        <source>Stallman drawing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/CardBacksLibrary.qml" line="84"/>
        <source>communism...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/CardBacksLibrary.qml" line="93"/>
        <source>eiffel tower</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/CardBacksLibrary.qml" line="102"/>
        <source>eiffel tower plan</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CardDeckModel</name>
    <message>
        <location filename="../qml/components/CardDeckModel.qml" line="35"/>
        <location filename="../qml/components/CardDeckModel.qml" line="36"/>
        <source>Café</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CardPage</name>
    <message>
        <location filename="../qml/pages/CardPage.qml" line="47"/>
        <source>About this card back...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/CardPage.qml" line="57"/>
        <source>Here is my bet...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="44"/>
        <source>About poker planning...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="48"/>
        <source>Settings...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/MainPage.qml" line="67"/>
        <source>Choose your card !</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="34"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="60"/>
        <source>Deck values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="65"/>
        <source>standard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="71"/>
        <source>fibonacci</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="77"/>
        <source>fibonacci with coffee</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="83"/>
        <source>custom series</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="95"/>
        <source>Custom series</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="96"/>
        <source>1,2,3,short text,...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="107"/>
        <source>Deck back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="128"/>
        <source>Shake to reveal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SettingsPage.qml" line="130"/>
        <source>Shake your Jolla to reveal your card !</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
