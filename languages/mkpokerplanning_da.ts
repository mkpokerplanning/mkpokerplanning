<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="da_DK">
<context>
    <name>AboutPage</name>
    <message>
        <source>About this app...</source>
        <translation>Om denne app...</translation>
    </message>
    <message>
        <source>&lt;b&gt;Play poker planning with your scrum team&lt;/b&gt;</source>
        <translation>Spil poker planning med dit scrum team.</translation>
    </message>
    <message>
        <source>The reason to use Planning poker is to avoid the influence of the other participants. If a number is spoken, it can sound like a suggestion and influence the other participants&apos; sizing. Planning poker should force people to think independently and propose their numbers simultaneously. This is accomplished by requiring that all participants show their card at the same time.</source>
        <translation>Grunden til at bruge poker planning, er at undgå indflydelse fra de andre deltagere. Hvis et nummer bliver sagt højt, kan det lyde som et forslag og påvirke de andre deltageres estimat. Planning poker skal tvinge folk til at tænke uafhængigt og foreslå deres nummer samtidigt. Dette opnås ved at kræve at alle deltagere viser deres kort på samme tid.</translation>
    </message>
    <message>
        <source>©2013 mecadu, the doubting mechanism.</source>
        <translation type="vanished">©2013 mecadu, den tvivlende mekanisme.</translation>
    </message>
    <message>
        <source>

Version 1.1-1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>
Distributed under GPLv3 license.
Source code available on Gitorious
https://gitorious.org/mkpokerplanning
©2013-2014 mecadu, the doubting mechanism.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CardBacksLibrary</name>
    <message>
        <source>random</source>
        <translation>tilfældigt</translation>
    </message>
    <message>
        <source>old style back</source>
        <translation>old style</translation>
    </message>
    <message>
        <source>swing project (orig.)</source>
        <translation>swing projekt (orig.)</translation>
    </message>
    <message>
        <source>project lifecycle (fr)</source>
        <translation>projekt livscyclus (fr)</translation>
    </message>
    <message>
        <source>brave GNU world</source>
        <translation>brave GNU world</translation>
    </message>
    <message>
        <source>Stallman drawing</source>
        <translation>Stallman tegning</translation>
    </message>
    <message>
        <source>communism...</source>
        <translation>kommunismen...</translation>
    </message>
    <message>
        <source>eiffel tower</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>eiffel tower plan</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CardDeckModel</name>
    <message>
        <source>Café</source>
        <translation>Café</translation>
    </message>
</context>
<context>
    <name>CardPage</name>
    <message>
        <source>Here is my bet...</source>
        <translation>Her der mit bud...</translation>
    </message>
    <message>
        <source>About this card back...</source>
        <translation>Om kort tilbage...</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>About poker planning...</source>
        <translation>Om planning poker</translation>
    </message>
    <message>
        <source>Settings...</source>
        <translation>Indstillinger...</translation>
    </message>
    <message>
        <source>Choose your card !</source>
        <translation>Vælg dine kort !</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <translation>Indstillinger</translation>
    </message>
    <message>
        <source>standard</source>
        <translation>standard</translation>
    </message>
    <message>
        <source>fibonacci</source>
        <translation>fibonacci</translation>
    </message>
    <message>
        <source>fibonacci with coffee</source>
        <translation>fibonacci med kaffe</translation>
    </message>
    <message>
        <source>Deck values</source>
        <translation>Kortværdier</translation>
    </message>
    <message>
        <source>Deck back</source>
        <translation>Kort tilbage</translation>
    </message>
    <message>
        <source>Shake to reveal</source>
        <translation>Ryst for at afslutte</translation>
    </message>
    <message>
        <source>Shake your Jolla to reveal your card !</source>
        <translation>Ryst din Jolla for at afsløre dine kort!</translation>
    </message>
    <message>
        <source>custom series</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Custom series</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>1,2,3,short text,...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
