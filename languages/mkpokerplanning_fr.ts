<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>AboutPage</name>
    <message>
        <source>About this app...</source>
        <translation>À propos...</translation>
    </message>
    <message>
        <source>&lt;b&gt;Play poker planning with your scrum team&lt;/b&gt;</source>
        <translation>Jouer au planning poker avec votre équipe scrum.</translation>
    </message>
    <message>
        <source>The reason to use Planning poker is to avoid the influence of the other participants. If a number is spoken, it can sound like a suggestion and influence the other participants&apos; sizing. Planning poker should force people to think independently and propose their numbers simultaneously. This is accomplished by requiring that all participants show their card at the same time.</source>
        <translation>La raison d&apos;être du planning poker est d&apos;éviter que les participants ne s&apos;influencent mutuellement. Si une évaluation est annoncée, cela aura un effet sur l&apos;évaluation des autres. Avec le planning poker, chacun dévoile sa carte en même temps, et est encouragé à réfléchir indépendamment.</translation>
    </message>
    <message>
        <source>

Version 1.1-1</source>
        <translation>

        Version 1.1-1</translation>
    </message>
    <message>
        <source>
Distributed under GPLv3 license.
Source code available on Gitorious
https://gitorious.org/mkpokerplanning
©2013-2014 mecadu, the doubting mechanism.</source>
        <translation>
        Distribué sous licence GPLv3.
        Code source disponible sur Gitorious
        https://gitorious.org/mkpokerplanning
        ©2013 mecadu, le mécanisme dubitatif.</translation>
    </message>
</context>
<context>
    <name>CardBacksLibrary</name>
    <message>
        <source>random</source>
        <translation>aléatoire</translation>
    </message>
    <message>
        <source>old style back</source>
        <translation>ancien</translation>
    </message>
    <message>
        <source>swing project (orig.)</source>
        <translation>Projet balançoire (orig.)</translation>
    </message>
    <message>
        <source>project lifecycle (fr)</source>
        <translation>cycle de vie</translation>
    </message>
    <message>
        <source>brave GNU world</source>
        <translation>brave GNU world</translation>
    </message>
    <message>
        <source>Stallman drawing</source>
        <translation>Dessin de Stallman</translation>
    </message>
    <message>
        <source>communism...</source>
        <translation>communisme...</translation>
    </message>
    <message>
        <source>eiffel tower</source>
        <translation>tour Eiffel</translation>
    </message>
    <message>
        <source>eiffel tower plan</source>
        <translation>plan de la tour Eiffel</translation>
    </message>
</context>
<context>
    <name>CardDeckModel</name>
    <message>
        <source>Café</source>
        <translation>Café</translation>
    </message>
</context>
<context>
    <name>CardPage</name>
    <message>
        <source>Here is my bet...</source>
        <translation>Mon évaluation...</translation>
    </message>
    <message>
        <source>About this card back...</source>
        <translation>À propos de ce dos de carte...</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>About poker planning...</source>
        <translation>À propos du planning poker</translation>
    </message>
    <message>
        <source>Settings...</source>
        <translation>Réglages...</translation>
    </message>
    <message>
        <source>Choose your card !</source>
        <translation>Choisissez votre carte !</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <translation>Réglages</translation>
    </message>
    <message>
        <source>standard</source>
        <translation>standard</translation>
    </message>
    <message>
        <source>fibonacci</source>
        <translation>fibonacci</translation>
    </message>
    <message>
        <source>fibonacci with coffee</source>
        <translation>fibonacci avec le café</translation>
    </message>
    <message>
        <source>Deck values</source>
        <translation>Valeurs du jeu</translation>
    </message>
    <message>
        <source>Deck back</source>
        <translation>Dos de cartes</translation>
    </message>
    <message>
        <source>Shake to reveal</source>
        <translation>Secouer pour montrer</translation>
    </message>
    <message>
        <source>Shake your Jolla to reveal your card !</source>
        <translation>Secouez votre Jolla pour révéler votre carte</translation>
    </message>
    <message>
        <source>custom series</source>
        <translation>série personnalisée</translation>
    </message>
    <message>
        <source>Custom series</source>
        <translation>Série personnalisée</translation>
    </message>
    <message>
        <source>1,2,3,short text,...</source>
        <translation>1,2,3,texte court,...</translation>
    </message>
</context>
</TS>
