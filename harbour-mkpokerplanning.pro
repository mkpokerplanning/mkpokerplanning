# The name of your app.
# NOTICE: name defined in TARGET has a corresponding QML filename.
#         If name defined in TARGET is changed, following needs to be
#         done to match new name:
#         - corresponding QML filename must be changed
#         - desktop icon filename must be changed
#         - desktop filename must be changed
#         - icon definition filename in desktop file must be changed
TARGET = harbour-mkpokerplanning

CONFIG += sailfishapp

SOURCES += src/harbour-mkpokerplanning.cpp

evil_hack_to_fool_lupdate {
    SOURCES += \
    qml/harbour-mkpokerplanning.qml \
    qml/cover/CoverPage.qml \
    qml/pages/MainPage.qml \
    qml/components/Card.qml \
    qml/pages/CardPage.qml \
    qml/pages/AboutPage.qml \
    qml/components/CardDeckModel.qml \
    qml/components/CardBacksLibrary.qml \
    qml/components/ShakeSensor.qml \
    qml/components/AppSettings.qml \
    qml/components/AboutBackPage.qml \
    qml/pages/SettingsPage.qml
}

OTHER_FILES += qml/harbour-mkpokerplanning.qml \
    qml/cover/CoverPage.qml \
    qml/pages/MainPage.qml \
    qml/pages/CardPage.qml \
    LICENSE.txt \
    changelog \
    qml/pages/SettingsPage.qml \
    languages/mkpokerplanning_fr.ts \
    languages/mkpokerplanning_fr.qm \
    harbour-mkpokerplanning.desktop \
    rpm/harbour-mkpokerplanning.yaml \
    qml/pages/ShakeSensor.qml \
    qml/components/ShakeSensor.qml \
    qml/components/Card.qml \
    qml/components/AppSettings.qml \
    qml/components/CardBacksLibrary.qml \
    qml/components/CardDeckModel.qml \
    qml/pages/AboutPage.qml \
    languages/mkpokerplanning_it.ts \
    languages/mkpokerplanning_fi.ts \
    languages/mkpokerplanning_es.ts \
    languages/mkpokerplanning_en.ts \
    languages/mkpokerplanning_de.ts \
    languages/mkpokerplanning_da.ts \
    qml/components/CardBack.qml \
    qml/components/CardBacksLibrary.qml \
    qml/pages/AboutBackPage.qml

RESOURCES += \
    images.qrc

HEADERS +=

TRANSLATIONS = languages/mkpokerplanning_fr.ts \
    languages/mkpokerplanning_en.ts \
    languages/mkpokerplanning_da.ts \
    languages/mkpokerplanning_fi.ts \
    languages/mkpokerplanning_es.ts \
    languages/mkpokerplanning_it.ts \
    languages/mkpokerplanning_de.ts

languages.path = /usr/share/harbour-mkpokerplanning/languages
languages.files = languages/*.qm

INSTALLS += languages

